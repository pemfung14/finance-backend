module DCF where

import GHC.Generics
import Data.Aeson

import Aws.Lambda

data DCFRequest = DCFRequest
  { eps1 :: Float
  , eps2 :: Float
  , eps3 :: Float
  , eps4 :: Float
  , eps5 :: Float
  , per1 :: Float
  , per2 :: Float
  , per3 :: Float
  , per4 :: Float
  , per5 :: Float
  } deriving (Generic)
instance FromJSON DCFRequest
instance ToJSON DCFRequest

handler :: DCFRequest -> Context -> IO (Either String Float)
handler dcfRequest context =
  if per1 dcfRequest > 0 && per2 dcfRequest > 0 && per3 dcfRequest > 0 && per4 dcfRequest > 0 && per5 dcfRequest > 0 && eps1 dcfRequest > 0 && eps2 dcfRequest > 0 && eps3 dcfRequest > 0 && eps4 dcfRequest > 0 && per5 dcfRequest > 0 then
    return (Right (hitungHargaWajarSaham (eps1 dcfRequest) (eps2 dcfRequest) (eps3 dcfRequest) (eps4 dcfRequest) (eps5 dcfRequest) (per1 dcfRequest) (per2 dcfRequest) (per3 dcfRequest) (per4 dcfRequest)  (per5 dcfRequest)))
  else
    return (Left "Input data is not valid!")

hitungHargaWajarSaham :: Float -> Float -> Float -> Float -> Float -> Float -> Float -> Float -> Float -> Float -> Float
hitungHargaWajarSaham eps1 eps2 eps3 eps4 eps5 per1 per2 per3 per4 per5 =
  let
    avgEps = avGrowthEPS eps1 eps2 eps3 eps4 eps5
    avgPer = avGrowthPER per1 per2 per3 per4 per5
    projEps1 = hitungProjectEPS eps5 avgEps
    projEps2 = hitungProjectEPS projEps1 avgEps
    projEps3 = hitungProjectEPS projEps2 avgEps
    projEps4 = hitungProjectEPS projEps3 avgEps
    projEps5 = hitungProjectEPS projEps4 avgEps
    hargaSahamAkhirTahun1 = kaliProjectPER projEps1 avgPer
    jumlahEPS = projEps1 + projEps2 + projEps3 + projEps4 + projEps5
    proyeksiDividen = jumlahEPS * 0.2
    hargaWajarSaham = (hargaSahamAkhirTahun1 + proyeksiDividen) / 1.11
  in hargaWajarSaham

avGrowthEPS :: Float -> Float -> Float -> Float -> Float -> Float
avGrowthEPS eps1 eps2 eps3 eps4 eps5 = (((eps2 - eps1)/eps1) + ((eps3 - eps2)/eps2) + ((eps4 - eps3)/eps3) + ((eps5 - eps4)/eps5))/5

avGrowthPER :: Float -> Float -> Float -> Float -> Float -> Float
avGrowthPER per1 per2 per3 per4 per5 = (per1+per2+per3+per4+per5)/5

projectEPS :: Float -> Float
projectEPS a
  | a > 0.15 = 1.15
  | otherwise = 1.1

projectPER :: Float -> Float
projectPER a
  | a > 20 = 17
  | otherwise = 12

hitungProjectEPS :: Float -> Float -> Float
hitungProjectEPS a avgEPS = a * projectEPS avgEPS-- projEps1 = hitungProjectEPS eps5-- projEps2 = hitungProjectEPS projEps1-- projEps3 = hitungProjectEPS projEps2-- projEps4 = hitungProjectEPS projEps3-- projEps5 = hitungProjectEPS projEps4

kaliProjectPER :: Float -> Float -> Float
kaliProjectPER a avgPER = a * projectPER avgPER
