module Graham where

import GHC.Generics
import Data.Aeson

import Aws.Lambda

data GrahamInput = GrahamInput
    { eps :: Float
    , growth :: Float
    } deriving (Generic)
instance FromJSON GrahamInput
instance ToJSON GrahamInput

handler :: GrahamInput -> Context -> IO (Either String Float)
handler grahamInput context =
    if eps grahamInput > 0 && growth grahamInput > 0 then
        return (Right ((eps grahamInput * (7 + growth grahamInput) * 7.8) / 11.4))
    else
        return (Left "Input data is not valid!")