module Pbv where

import GHC.Generics
import Data.Aeson
import Aws.Lambda
import Data.List

data PBVRequest = PBVRequest
  { pbv1 :: Float
  , pbv2 :: Float
  , pbv3 :: Float
  , pbv4 :: Float
  , pbv5 :: Float
  , bvps :: Float
  } deriving (Generic)
instance FromJSON PBVRequest
instance ToJSON PBVRequest

average xs = realToFrac (sum xs) / genericLength xs

handler :: PBVRequest -> Context -> IO (Either String Float)
handler pbvRequest context =
  if pbv1 pbvRequest > 0 then
    return (Right ( (average [pbv1 pbvRequest,pbv2 pbvRequest,pbv3 pbvRequest,pbv4 pbvRequest,pbv5 pbvRequest]) * bvps pbvRequest))
  else
    return (Left "PBV Input data is not valid!")
