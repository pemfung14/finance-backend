module PER where

import GHC.Generics
import Data.Aeson

import Aws.Lambda

data PERInput = PERInput
 { per1 :: Float
 , per2 :: Float
 , per3 :: Float
 , per4 :: Float
 , per5 :: Float
 , epsttm :: Float
 } deriving (Generic)
instance FromJSON PERInput
instance ToJSON PERInput

perAnalysis :: Float -> Float -> Float -> Float -> Float -> Float -> Float
perAnalysis per1 per2 per3 per4 per5 eps = ((per1+per2+per3+per4+per5) / 5) * eps

handler :: PERInput -> Context -> IO (Either String Float)
handler perInput context =
  if per1 perInput > 0 && per2 perInput > 0 && per3 perInput > 0 && per4 perInput > 0 && per5 perInput > 0 then
    return (Right (perAnalysis (per1 perInput) (per2 perInput) (per3 perInput) (per4 perInput) (per5 perInput) (epsttm perInput)))
  else
    return (Left "Input data is not valid!")